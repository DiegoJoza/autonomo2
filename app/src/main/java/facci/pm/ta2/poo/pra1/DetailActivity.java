package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    private View ACVER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        ACVER = findViewById(R.id.progress);



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        final String object_id = getIntent().getStringExtra("objet_id");

        DataQuery  query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {

                    TextView price = findViewById(R.id.price);
                    price.setText((String) object.get("price"));

                    TextView description =  findViewById(R.id.description);
                    description.setText((String) object.get("description"));

                    TextView title = findViewById(R.id.title);
                    title.setText((String) object.get("name"));

                    ImageView thumbnail = findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));



                    description.setMovementMethod(new ScrollingMovementMethod());
                }
            else

                {

                    //Error
                }
            }
        });

        // FIN - CODE6

    }

}
